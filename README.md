#### The requirement
In a database there is a table that contains temporal values of some counters
(for example we can consider that these are some metrics of a web server - the number of requests being processed at that moment).
These values are written periodically (every second) by a third party process.

It is required to make a solution by which the data in this table are aggregated continuously and every minute to generate the average value and the sum of the values from that minute.
Aggregates (sum and average) must be written to another database.

It is desired that the aggregation process be highly available.
You must propose a design that takes this into account and you will need to explain how the proposed design meets the resilience requirement.

Optional: Aggregate data must be displayed as a REST endpoint.

#### The proposed solution
Data from the existing database (table) is collected by Kafka Connect with the Debezium source connector and sent to a Kafka topic.
With Kafka Streams the data is aggregated and through Kafka Connect with Cassandra sink connector are saved in Cassandra.
The aggregated data is displayed through a REST microservice.

![System architecture design](https://gitlab.com/the-aggregator/docs/-/raw/master/System%20architecture%20design.png "System architecture design")

#### Aspects regarding the availability and resilience of the system.
If the source connector (Debezium) has stopped gracefully, it finishes its current work by finally saving the position / offset in Kafka.
When the connector is restarted, it will continue its activity from where it was previously.
And if it stops unexpectedly, it will be restarted, but it will continue its activity from the last position registered in Kafka before termination,
resulting in possible duplicate events. For deduplication, the kafka streams process checks each message to have the timestamp
strictly higher than the last processed / aggregated message.
The same thing happens with the sink connector (Cassandra).

If there are problems with the Kafka cluster and one of the brokers is not available,
then another leader will be chosen from the existing synchronized replicas.
But if the minimum number of synchronized replicas cannot be met,
then the connectors that write or those that read will be locked until Kafka brokers can be restarted or recreated.
Therefore the minimum number of synchronized replicas has a direct impact on availability and, for reasons of data consistency, should be greater than 1.

Similarly, if one of the nodes in the Cassandra cluster is not available,
the load will be taken over by another node that is available and has a replica for the partition key
if the replication factor is greater than 1. Otherwise, queries on the partitions managed by this node will fail, but not for all partitions.

Therefore, availability increases with the number of Kafka brokers and Cassandra nodes and in addition increases when the replication factor is higher (both in Kafka and in Cassandra).
However, when:

1.  source connector is not available results in a delayed update for all new metrics
2. 
    - a kafka broker is not available the load is taken over by the other brokers that contain the replicas.
    
    - a kafka broker is not available and there is no replica, it results in a delayed update for the metrics aggregates from certain minutes (partial availability).
    
    - the kafka cluster is not accessible, update totally delayed.
3.
    - a task in the sink connector is not available, update partially delayed.
  
    - a single instance of Kafka Connect and unavailable, totally delayed update.
4.
    - Cassandra node unavailable and no replica, update partially delayed.
    
    - Cassandra cluster unavailable, update totally delayed.

Partially delayed update = only metric aggregates in certain minutes are updated late.

Totally delayed update = updating all metric aggregates for minutes of unavailability are delayed until availability returns.

### Project content
api - the microservice that exposes the aggregates of metrics through REST API.

aggregator - The process of Kafka Streams for aggregating metrics.

connector - Simulation of the source connector and sink connector.

docs - repo with this document
