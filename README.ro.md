#### Cerinta
Intr-o baza de date exista o tabela ce contine valori temporale ale unor contori 
(spre exemplu putem considera ca este vorba despre niste metrici ale unui server web – numarul de requesturi in procesare momentul respectiv). 
Aceste  valori sunt scrise periodic (la fiecare secunda) de un proces tert. 

Se cere sa faci o solutie prin care datele din aceasta tabela sa fie aggregate in mod continuu si la fiecare minut sa se genereze valoare medie cat si suma valorilor din acel minut.
Agregatele (suma si media) trebuie scrise intr-o alta baza de date. 

Se doreste ca procesul de agregare sa fie highly available. 
Trebuie sa propui un design care sa tina cont de acest aspect si va trebui sa explici cum designul propus satisfice cerinta de rezilienta. 

Optional: Datele aggregate trebuie expuse ca un endpoint REST. 

#### Solutia propusa
Datele din baza de date (tabela) existenta sunt colectate de Kafka Connect cu Debezium source connector si trimise pe un topic de Kafka.
Cu Kafka Streams se agrega datele si prin Kafka Connect cu Cassandra sink connector sunt salvate in Cassandra.
Datele agregate sunt expuse printr-un microserviciu REST.

![System architecture design](https://gitlab.com/the-aggregator/docs/-/raw/master/System%20architecture%20design.png "System architecture design")

#### Aspecte privind disponibilitatea si rezilienta sistemului.
Daca source connector-ul (Debezium) s-a oprit gratios, isi termina lucrul curent salvand la final pozitia/ofsetul in Kafka.
Cand conectorul este restartat, isi va continua activitatea de unde a ramas anterior.
Iar daca acesta se opreste neasteptat, va fi restartat, dar isi va continua activitatea de la ultima pozitie inregistrata in Kafka inainte de terminare,
rezultand in posibile evenimente duplicate. Pentru deduplicare, procesul de kafka streams verifica fiecare mesaj sa aiba timestamp-ul 
mai mare strict decat ultimul mesaj procesat/agregat. 
Acelasi lucru se intampla si cu sink connector-ul (Cassandra).

In situatia cand sunt probleme cu cluster-ul de Kafka si unul din brokeri nu este disponibil, 
atunci se va alege un alt lider din replicile sincronizte existente. 
Insa daca nu se poate satisface numarul minim de replici sincronizate, 
atunci conectorii care scriu sau cei care citesc se vor bloca pur pana cand brokerii Kafka pot fi reporniti sau recreati. 
De aceea numarul minim de replici sincronizate are un impact direct asupra disponibilitatii si, din motive de consistenta de date, ar trebui sa fie mai mare de 1.

Similar, daca unul din nodurile din clusterul de Cassandra nu este disponibil, 
load-ul va fi preluat de alt nod care este disponibil si are o replica pentru cheia de partitie 
in cazul in care factorul de replicare este mai mare de 1. Daca nu, query-urile pe partitiile gestionate de acest nod vor esua, dar nu pentru toate partitiile.

Asadar, disponibilitate creste cu numarul de brokeri Kafka si noduri Cassandra si in plus creste cand factorul de replicare este mai mare (atat in Kafka cat si in Cassandra).
Totusi, cand:

1. source connectorul nu e disponibil rezulta intr-un update intarziat pentru toate metricile noi
2. 
    - un broker kafka nu este disponibil load-ul este preluat de ceilalti brokeri care contin replicile. 
    
    - un broker kafka nu este disponibil si nu exista nicio replica, rezulta intr-un update intarziat pentru agregatele metricilor din anumite minute (disponibilitate partiala). 
    
    - clusterul de kafka nu este accesibil, update intarziat total.
3.
    - un task din sink connector nu e disponibil, update intarziat partial.
  
    - o singura instanta Kafka Connect si aceea indisponibila, update intarziat total.
4.
    - nod Cassandra indisponibil si nicio replica, update intarziat partial.
    
    - cluster Cassandra indisponibil, update intarziat total.

Update intarziat partial = doar agregatele metricilor din anumite minute sunt actualizate cu intarziere.

Update intarziat total = actualizarea tuturor agregatelor metricilor pentru minutele de indisponibilitate sunt intarziate pana la revenirea disponibilitatii.

### Descrierea surselor
api - microserviciul care expune agragatele metricilor printr-un REST API.

aggregator - Procesul de Kafka Streams pentru agregarea metricilor.

connector - Simularea source connector-ului si sink connector-ului.

docs - repo cu acest document
